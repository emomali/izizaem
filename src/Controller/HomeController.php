<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class HomeController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        $number = 100;
        return $this->render('base.html.twig', [
            'number' => $number,
        ]);
    }

}